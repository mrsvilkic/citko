const questions = [
  {
    id: 1,
    question: "U poređenju sa prosečnim, dobri čitaoci čitaju? ",
    answers: [
      { id: 1, text: "Većom brzinom i bolje razumeju pročitano" },
      { id: 2, text: "Većom brzinom i isto razumeju pročitano kao i prosečan" },
      { id: 3, text: "Većom brzinom i lošije razumeju pročitano" },
    ],
    correct: 1,
  },
  {
    id: 2,
    question: "Čitaoci koji čitaju preko 1000 rpm?",
    answers: [
      { id: 1, text: "Čine prosečne čitaoce" },
      { id: 2, text: "Čine većinu čitaoca" },
      { id: 3, text: "Čine 1 % manjine" },
    ],
    correct: 3,
  },
  {
    id: 3,
    question: "Prosečna brzina čitanja je oko?",
    answers: [
      { id: 1, text: "300 rpm" },
      { id: 2, text: "200 rpm" },
      { id: 3, text: "150 rpm" },
    ],
    correct: 2,
  },
  {
    id: 4,
    question: "Razumevanje prosečnog čitaoca je oko?",
    answers: [
      { id: 1, text: "50%" },
      { id: 2, text: "60%" },
      { id: 3, text: "85%" },
    ],
    correct: 2,
  },
  {
    id: 5,
    question:
      "Sprinter koji trči brzinom kojom prosečan čitalac čita, pretrči 100m za?",
    answers: [
      { id: 1, text: "10 sec (skoro rekordno vreme)" },
      { id: 2, text: "35 sec (prosečno)" },
      { id: 3, text: "70 sec (brzina hodanja)" },
    ],
    correct: 3,
  },
  {
    id: 6,
    question:
      "Koji je najefektivniji način da se pridobije znanje iz informacije?",
    answers: [
      { id: 1, text: "watching TV" },
      { id: 2, text: "reading text" },
      { id: 3, text: "listening to a speaker" },
    ],
    correct: 2,
  },
  {
    id: 7,
    question: "Prosečna brzina govora daktilografa iznosi?",
    answers: [
      { id: 1, text: "120 rpm" },
      { id: 2, text: "150 rpm" },
      { id: 3, text: "200 rpm" },
    ],
    correct: 2,
  },
  {
    id: 8,
    question: "Većina korisnika računara želi da?",
    answers: [
      { id: 1, text: "Popravi brzinu kucanja" },
      { id: 2, text: "Popravi brzinu čitanja" },
      { id: 3, text: "Kupi veliki televizor" },
    ],
    correct: 1,
  },
  {
    id: 9,
    question: "Šta će smanjiti vrednost brzine kucanja?",
    answers: [
      { id: 1, text: "Programi za ispravljanje grešaka u kucanju" },
      { id: 2, text: "Bolje tastature" },
      { id: 3, text: "Programi za prepoznavanje glasa" },
    ],
    correct: 3,
  },
  {
    id: 10,
    question: "Šta nedostaje seminaru ili knjizi o brzom čitanju?",
    answers: [
      { id: 1, text: "Brzi rezultati" },
      { id: 2, text: "Konstantno vežbanje" },
      { id: 3, text: "Dovoljno objašnjenja" },
    ],
    correct: 2,
  },
];

export default questions;
