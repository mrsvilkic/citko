import React, { useState, useEffect, useRef } from "react";
import defaultText from "../data/spritzText";
import { Button, Container, Dropdown, Form } from "react-bootstrap";

const SpeedApp = () => {
  const [text, setText] = useState("");
  const [index, setIndex] = useState(-1);
  const [rpm, setRpm] = useState(400);
  const [word, setWords] = useState([]);
  const countRef = useRef(null);
  const [isReading, setIsReading] = useState(false);

  useEffect(() => {
    setText(defaultText);
    setWords(defaultText.split(" "));
  }, []);

  useEffect(() => {
    stopReading();
  }, [rpm]);

  const startReading = () => {
    setIsReading(true);
    countRef.current = setInterval(() => {
      setIndex((index) => index + 1);
    }, rpm);
  };

  const stopReading = () => {
    setIsReading(false);
    clearInterval(countRef.current);
  };

  const resetRead = () => {
    stopReading();
    setWords(text.split(" "));
    setIndex(-1);
  };

  const showFile = () => {
    var preview = document.getElementById("show-text");
    var file = document.querySelector("input[type=file]").files[0];
    var reader = new FileReader();

    var textFile = /text.*/;

    if (file.type.match(textFile)) {
      reader.onload = function (event) {
        setText(event.target.result);
        setWords(event.target.result.split(" "));
        stopReading();
        setIndex(0);
      };
    } else {
      preview.innerHTML =
        '<div class="alert alert-danger">Fajl mora biti .txt</div>';
    }
    reader.readAsText(file);
  };

  const uploadFile = () => {
    document.getElementById("fileInput").click();
  };
  return (
    <Container className="p-4 " style={{ backgroundColor: "#eee" }}>
      <h2>
        <strong>Vežbajte brzo čitanje online</strong>
      </h2>
      <p>
        Ovde možete vežbati brzo čitanje online. Možete dodati svoj tekst a
        možete i vežbati na predefinisanim tekstom koji se može videti ispod.
      </p>

      <p>
        Dugme <strong>Počni</strong> započinje prikazivanje reči. Dugme{" "}
        <strong>Stani</strong> pauzira prikazivanje reči.
      </p>
      <p>
        Dugme <strong>Reset</strong> resetuje na početak teksta. Iz liste{" "}
        <strong>150 rpm</strong> možete odabrati brzinu čitanja teksta.
      </p>
      <p>Tekst fajl koji učitate mora biti .txt formata.</p>
      <Button onClick={uploadFile}> Učitaj svoj tekst fajl</Button>
      <Form.File id="fileInput" onChange={showFile} hidden />
      <hr />
      <div className="p-4 shadow-lg" style={{ backgroundColor: "#fff" }}>
        <div className="col col-xs-12 col-sm-12 col-md-6 m-auto">
          <div className="text-center border-top border-bottom py-4 my-4">
            <h5 className="mb-0">
              {index < 0 ? (
                <span className="text-danger">
                  Pritisnite dugme <i>Počni</i>
                </span>
              ) : (
                ""
              )}
              {word[index]}
              {/* {index > 0 && recPoPrikazu === 2 ? " " + word[index + 1] : ""} */}
              {/* {index > 0 && recPoPrikazu === 3 ? " " + word[index + 2] : ""} */}
            </h5>
          </div>
          <div className="d-flex justify-content-center">
            <Button
              className="mr-3"
              onClick={startReading}
              disabled={isReading}
            >
              Počni
            </Button>
            <Button
              variant="danger"
              className="mr-3"
              onClick={stopReading}
              disabled={!isReading}
            >
              Stani
            </Button>
            <Button
              variant="warning"
              className="mr-3"
              onClick={resetRead}
              disabled={index >= 0 ? false : true}
            >
              Reset
            </Button>

            <Dropdown className="d-inline mr-3">
              <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                {60 / (rpm / 1000)} rpm
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item onClick={() => setRpm((60 / 150) * 1000)}>
                  150 rpm
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setRpm((60 / 200) * 1000)}>
                  200 rpm
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setRpm((60 / 250) * 1000)}>
                  250 rpm
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setRpm((60 / 300) * 1000)}>
                  300 rpm
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setRpm((60 / 400) * 1000)}>
                  400 rpm
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setRpm((60 / 500) * 1000)}>
                  500 rpm
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setRpm((60 / 550) * 1000)}>
                  550 rpm
                </Dropdown.Item>
                <Dropdown.Item onClick={() => setRpm((60 / 600) * 1000)}>
                  600 rpm
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>

            {/* <Dropdown className="d-inline">
          <Dropdown.Toggle variant="secondary" id="dropdown-basic">
            {recPoPrikazu} reč
          </Dropdown.Toggle>

          <Dropdown.Menu>
            <Dropdown.Item onClick={() => setRecPoPrikazu(1)}>
              1 reč
            </Dropdown.Item>
            <Dropdown.Item onClick={() => setRecPoPrikazu(2)}>
              2 reči
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown> */}
          </div>
        </div>
      </div>
      <div className="border p-4 m-2" id="show-text">
        <h5>Pregled teksta:</h5>
        {text}
      </div>
    </Container>
  );
};

export default SpeedApp;
