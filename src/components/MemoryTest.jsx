import React from "react";
import { ListGroup } from "react-bootstrap";

const MemoryTest = ({ questions, answers, reading, checkQuestion }) => {
  const questionVariant = (q, a) => {
    if (answers.find((num) => num === q)) {
      if (questions[q - 1].correct === a) return "primary";
      return "secondary";
    } else return "";
  };

  return (
    <div id="pamcenje" hidden={!reading}>
      <h2>
        <strong>2. Test razumevanja </strong>
      </h2>
      <p>
        U ovom testu je potrebno setiti se detalja iz prethodnog teksta i
        odgovoriti na data pitanja. Ovaj test nije ograničen vremenom.
      </p>
      <div className="row">
        {questions.map((question) => (
          <div key={question.id} className="col col-12 col-sm-12 col-lg-6 my-3">
            <h5 style={{ height: "50px" }}>
              <strong>
                {question.id}. {question.question}
              </strong>
            </h5>
            <ListGroup className="">
              {question.answers.map((ans) => (
                <ListGroup.Item
                  key={ans.id}
                  action={!answers.find((num) => num === question.id)}
                  variant={questionVariant(question.id, ans.id)}
                  onClick={() => checkQuestion(question.id, ans.id)}
                >
                  {ans.id}. {ans.text}
                </ListGroup.Item>
              ))}
            </ListGroup>
          </div>
        ))}
      </div>
    </div>
  );
};

export default MemoryTest;
