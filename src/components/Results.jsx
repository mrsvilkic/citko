import React from "react";
import { Alert, Table } from "react-bootstrap";

const Results = ({ reading, finished, speed, time, score }) => {
  return (
    <div id="tabela" className="my-4">
      <h3>
        <strong>Rezultati</strong>
      </h3>
      <Alert className="p-5" hidden={!(reading && finished)} variant="success">
        Vaša brzina čitanja je <strong>{speed}</strong> reči po minutu (rpm).
        576 reči ste pročitali za <strong>{time()}</strong>. Razumeli ste{" "}
        <strong>{score * 10}% </strong> pročitanog teksta.
      </Alert>
      <p>
        Merenja brzine i razumevanja zavise od sadržaja teksta i od niza
        pitanja. Rezultati u tabeli ne odgovaraju određenom testu, ali daju
        opštu predstavu o efikasnosti čitanja.
      </p>
      <p>
        Takođe imajte na umu da je čitanje sa ekrana znatno sporije od čitanja
        sa papira.
      </p>
      <Table striped bordered>
        <thead>
          <tr>
            <th>Ekran</th>
            <th>Papir</th>
            <th>Pamćenje</th>
            <th>Rezultat</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>100 rpm</td>
            <td>110 rpm</td>
            <td>50%</td>
            <td>Ispod proseka</td>
          </tr>
          <tr>
            <td>200 rpm</td>
            <td>240 rpm</td>
            <td>60%</td>
            <td>Prosečno</td>
          </tr>
          <tr>
            <td>300 rpm</td>
            <td>400 rpm</td>
            <td>80%</td>
            <td>Solidno</td>
          </tr>
          <tr>
            <td>700 rpm</td>
            <td>1000 rpm</td>
            <td>85%</td>
            <td>Iznad proseka</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default Results;
