import React from "react";

const Instrukcije = () => {
  return (
    <div id="instrukcije" className="my-5">
      <h2>
        <strong>Instrukcije testa</strong>
      </h2>
      <p>
        Pripremite se za čitanje. Kada ste spremni, kliknete na dugme
        <strong>
          <i> Započni</i>
        </strong>{" "}
        i započnite čitanje. Dugme pokreće tajmer. Ne ubrzavajte, već čitajte
        normalno da biste pronašli svoj trenutni nivo čitanja.
      </p>
      <p>
        Pritisnite dugme{" "}
        <strong>
          <i> Završi </i>
        </strong>{" "}
        čim završite sa čitanjem. Ovo će zaustaviti tajmer. Pojaviće se pitanja
        na koja treba da odgovorite.
      </p>
      <p>
        Nakon što završite sa čitanjem, pojaviće se test na kome se proverava
        koliko ste upamtili od pročitanog.
      </p>
      <p>
        Kada završite i drugi test pojaviće se vaša brzina čitanja kao i
        procenat koliko ste razumeli.
      </p>
    </div>
  );
};

export default Instrukcije;
