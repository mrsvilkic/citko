import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";

const CustomNavbar = () => {
  return (
    // Navigacija
    <Navbar bg="dark" variant="dark">
      <Container>
        <Navbar.Brand href="/">Čitko</Navbar.Brand>
        <Nav className="mr-auto">
          <Nav.Link>
            <NavLink className="text-white" to="/test">
              Početna
            </NavLink>
          </Nav.Link>

          <Nav.Link>
            <NavLink className="text-white" to="/vezba">
              Vezbaj
            </NavLink>
          </Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default CustomNavbar;
