import React from "react";
import { Route, Switch } from "react-router-dom";
import { Container } from "react-bootstrap";
import ReadingScreen from "./ReadingScreen";
import CustomNavbar from "./components/CustomNavbar";
import SpeedApp from "./components/SpeedApp";

const App = () => {
  return (
    <>
      <Container className="text-center">
        <h1>Test brzine čitanja</h1>
      </Container>
      <CustomNavbar />
      <Switch>
        <Route path="/vezba" component={SpeedApp} />
        <Route path="/test" component={ReadingScreen} />
        <Route path="/" component={ReadingScreen} />
      </Switch>
    </>
  );
};

export default App;
