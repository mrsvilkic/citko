import React, { useRef, useState } from "react";
import { Container } from "react-bootstrap";
import ReadingTest from "./components/ReadingTest";
import Uvod from "./components/Uvod";
import Instrukcije from "./components/Instrukcije";
import MemoryTest from "./components/MemoryTest";
import Results from "./components/Results";
import questions from "./data/questions";

const ReadingScreen = () => {
  const [timer, setTimer] = useState(0);
  const [points, setPoints] = useState(0);
  const [isActive, setIsActive] = useState(false);
  const [isFinished, setIsFinished] = useState(false);
  const [questionsAnswered, setQuestionsAnswered] = useState(0);
  const [quizFinished, setQuizFinished] = useState(false);
  const countRef = useRef(null);

  const [readingSpeed, setReadingSpeed] = useState(0);
  const [answers, setAnswers] = useState([]);

  const checkQuestion = (q, a) => {
    if (!answers.find((num) => num === q)) {
      if (questions[q - 1].correct === a) setPoints(points + 1);
      setAnswers([...answers, q]);
      const qA = questionsAnswered + 1;
      setQuestionsAnswered(questionsAnswered + 1);
      if (qA === 10) setQuizFinished(true);
    }
  };

  const handleStart = () => {
    setTimer(0);
    setPoints(0);
    setQuestionsAnswered(0);
    setAnswers([]);
    setIsActive(true);
    setIsFinished(false);
    setQuizFinished(false);
    countRef.current = setInterval(() => {
      setTimer((timer) => timer + 1);
    }, 1000);
  };

  const handleStop = () => {
    setIsActive(false);
    setIsFinished(true);
    clearInterval(countRef.current);
    setReadingSpeed(Math.round((60 / timer) * 574));
  };

  const getTime = () => {
    const min = Math.floor(timer / 60);
    const sec = Math.floor(timer % 60);
    return `${min}min :${sec}sec`;
  };

  return (
    <>
      <Container className="p-4">
        <Uvod />

        {/* Instrukcije deo */}
        <Instrukcije />

        {/* Test deo*/}
        <ReadingTest
          onStart={handleStart}
          onStop={handleStop}
          reading={isActive}
        />

        {/* Pamcenje */}
        <MemoryTest
          questions={questions}
          answers={answers}
          reading={isFinished}
          checkQuestion={checkQuestion}
        />

        {/* Tabela rezultata */}
        <Results
          reading={isFinished}
          finished={quizFinished}
          speed={readingSpeed}
          time={getTime}
          score={points}
        />
      </Container>
    </>
  );
};

export default ReadingScreen;
